# Bookstack

This repo contains the helmchart and configuration for S&T's Bookstack instance.

> Note: The `.env` file **DOES NOT** get pushed to live configuration at this time. It is included to track changes, but any edits must be deployed manually.

## Future Enhancements

* Create backup/restore scripts
* Create .env deployment script

## Configuration Notes

Here are some notes that may be helpful when deploying a fresh instance of Bookstack:

* When configuring authentication, you will need to grant a group admin rights over the instance. To do this, first disable SSO authentication and login with the default admin account. (Make sure you change this password.) After you have logged in, reenable SSO authentication. Now that SSO is enabled, go to the Roles tab of Settings and select the built in Admin role. You will see a new field `External Athentication IDs`. Set this field to the name of the group you want to have admin rights. For example, `it-si`. Log out of the built in admin account, and now everyone in the group should have global admin rights.
* When restoring backups between environments, or when the instance URL changes, you will need to migrate the URLs in the database. There are some basic instructions here: <https://www.bookstackapp.com/docs/admin/commands/#update-system-url>. Since we are using the container version in k8s, if you attempt to run this command it will fail with an authentication error. This is because the DB_PASSWORD environment variable is not set in the context you are using when you exec into the container. Set the DB_PASSWORD first, and then run the command.

## Overlay
Since the original image uses a volume command, we can't actually copy files into it with a new dockerfile. These files are provided here for the sake of tracking changes.