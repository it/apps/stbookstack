<?php

use BookStack\Facades\Theme;
use BookStack\Theming\ThemeEvents;

// Listen to OIDC ID Token pre validation event so that we can handle token data
Theme::listen(ThemeEvents::OIDC_ID_TOKEN_PRE_VALIDATE, function (array $idTokenData, array $accessTokenData) {

    // Get the existing email from the data
    $email = $idTokenData['email'] ?? null;

    // If there's no email value found, add one using the 'name' value.
    if (!$email) {
        $idTokenData['email'] = $idTokenData['name'] . '@mst.edu';
    }

    // Return our tweaked token data
    return $idTokenData;
});
